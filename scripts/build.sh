#!/bin/bash
./gradlew assembleQuickstep assemblePixel
mkdir -p apks
mv -f app/build/outputs/apk/quickstep/app-quickstep.apk apks/Quickstep.apk
mv -f app/build/outputs/apk/pixel/app-pixel.apk apks/Pixel.apk
